package org.osra.app.service.listImport;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.osra.app.domain.Orphan;

public class OrphanListImportResults {

	public OrphanListImportResults() {
		orphans = new ArrayList<Orphan>();
		validationErrors = new ArrayList<ImportException>();
	}

	public List<Orphan> getOrphans() {
		return orphans;
	}

	public List<ImportException> getValidationErrors() {
		return validationErrors;
	}

	private List<Orphan> orphans;

	private List<ImportException> validationErrors;

	public void addOrphan(Orphan o) {
		orphans.add(o);
	}

	public void addValidationError(ImportException e) {
		validationErrors.add(e);
	}

	public void addMultiValidationError(List<ImportException> l) {
		validationErrors.addAll(l);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}

	
	
}
