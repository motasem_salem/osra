package org.osra.app.service.listImport;

public interface OrphanDataRecord {

	public String getReference();

	public OrphanDataField getFirstNameField();

	public OrphanDataField getFatherNameField();

	public OrphanDataField getFatherMartyrField();

	public OrphanDataField getFatherOccupationField();

	public OrphanDataField getFatherPlaceOfDeathField();

	public OrphanDataField getFatherCauseOfDeathField();

	public OrphanDataField getFatherDateOfDeathField();

	public OrphanDataField getMotherNameField();

	public OrphanDataField getMotherAliveField();

	public OrphanDataField getDateOfBirthField();

	public OrphanDataField getGenderField();

	public OrphanDataField getHealthStatusField();

	public OrphanDataField getSchoolingStatusField();

	public OrphanDataField getGoesToSchoolField();

	public OrphanDataField getGuardianNameField();

	public OrphanDataField getGuardianRelationshipField();

	public OrphanDataField getGuardianIDNumberField();

	public OrphanDataField getOriginalAddressGovField();

	public OrphanDataField getOriginalAddressCityField();

	public OrphanDataField getOriginalAddressNeighborhoodField();

	public OrphanDataField getOriginalAddressStreetField();

	public OrphanDataField getOriginalAddressDetailsField();
	
	public OrphanDataField getCurrentAddressGovField();

	public OrphanDataField getCurrentAddressCityField();

	public OrphanDataField getCurrentAddressNeighborhoodField();

	public OrphanDataField getCurrentAddressStreetField();

	public OrphanDataField getCurrentAddressDetailsField();
	
	public OrphanDataField getContactNumberField();

	public OrphanDataField getAlternativeContactNumberField();

	public OrphanDataField getSponsoredByAnotherOrgField();

	public OrphanDataField getAnotherOrgSponsorshipDetailsField();

	public OrphanDataField getMinorSiblingsCountField();

	public OrphanDataField getSponsoredMinorSiblinbgsCountField();

	public OrphanDataField getCommentsField();

}
