package org.osra.app.service.listImport;

import java.util.Iterator;

import org.osra.app.domain.Orphan;

public interface OrphanListDataSource extends Iterator<OrphanDataRecord> {
	
	public void init() throws ImportException;
	
	

}
