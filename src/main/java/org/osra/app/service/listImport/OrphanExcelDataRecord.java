package org.osra.app.service.listImport;

import org.apache.poi.ss.usermodel.Row;

public class OrphanExcelDataRecord implements OrphanDataRecord {

	private Row myRow;

	// return new
	// OrphanExcelDataField(myRow.getCell(0,Row.CREATE_NULL_AS_BLANK));

	public OrphanExcelDataRecord(Row myRow) {
		super();
		this.myRow = myRow;
	}

	@Override
	public String getReference() {
		return "Row:" + myRow.getRowNum();
	}

	@Override
	public OrphanDataField getFirstNameField() {
		return new OrphanExcelDataField(myRow.getCell(1,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getFatherNameField() {
		return new OrphanExcelDataField(myRow.getCell(2,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getFatherMartyrField() {
		return new OrphanExcelDataField(myRow.getCell(3,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getFatherOccupationField() {
		return new OrphanExcelDataField(myRow.getCell(4,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getFatherPlaceOfDeathField() {
		return new OrphanExcelDataField(myRow.getCell(5,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getFatherCauseOfDeathField() {
		return new OrphanExcelDataField(myRow.getCell(6,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getFatherDateOfDeathField() {
		return new OrphanExcelDataField(myRow.getCell(7,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getMotherNameField() {
		return new OrphanExcelDataField(myRow.getCell(8,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getMotherAliveField() {
		return new OrphanExcelDataField(myRow.getCell(9,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getDateOfBirthField() {
		return new OrphanExcelDataField(myRow.getCell(10,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getGenderField() {
		return new OrphanExcelDataField(myRow.getCell(11,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getHealthStatusField() {
		return new OrphanExcelDataField(myRow.getCell(12,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getSchoolingStatusField() {
		return new OrphanExcelDataField(myRow.getCell(13,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getGoesToSchoolField() {
		return new OrphanExcelDataField(myRow.getCell(14,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getGuardianNameField() {
		return new OrphanExcelDataField(myRow.getCell(15,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getGuardianRelationshipField() {
		return new OrphanExcelDataField(myRow.getCell(16,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getGuardianIDNumberField() {
		return new OrphanExcelDataField(myRow.getCell(17,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getOriginalAddressGovField() {
		return new OrphanExcelDataField(myRow.getCell(18,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getOriginalAddressCityField() {
		return new OrphanExcelDataField(myRow.getCell(19,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getOriginalAddressNeighborhoodField() {
		return new OrphanExcelDataField(myRow.getCell(20,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getOriginalAddressStreetField() {
		return new OrphanExcelDataField(myRow.getCell(21,
				Row.CREATE_NULL_AS_BLANK));
	}
	
	@Override
	public OrphanDataField getOriginalAddressDetailsField() {
		return new OrphanExcelDataField(myRow.getCell(22,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getCurrentAddressGovField() {
		return new OrphanExcelDataField(myRow.getCell(23,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getCurrentAddressCityField() {
		return new OrphanExcelDataField(myRow.getCell(24,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getCurrentAddressNeighborhoodField() {
		return new OrphanExcelDataField(myRow.getCell(25,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getCurrentAddressStreetField() {
		return new OrphanExcelDataField(myRow.getCell(26,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getCurrentAddressDetailsField() {
		return new OrphanExcelDataField(myRow.getCell(27,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getContactNumberField() {
		return new OrphanExcelDataField(myRow.getCell(28,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getAlternativeContactNumberField() {
		return new OrphanExcelDataField(myRow.getCell(29,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getSponsoredByAnotherOrgField() {
		return new OrphanExcelDataField(myRow.getCell(30,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getAnotherOrgSponsorshipDetailsField() {
		return new OrphanExcelDataField(myRow.getCell(31,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getMinorSiblingsCountField() {
		return new OrphanExcelDataField(myRow.getCell(32,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getSponsoredMinorSiblinbgsCountField() {
		return new OrphanExcelDataField(myRow.getCell(33,
				Row.CREATE_NULL_AS_BLANK));
	}

	@Override
	public OrphanDataField getCommentsField() {
		return new OrphanExcelDataField(myRow.getCell(34,
				Row.CREATE_NULL_AS_BLANK));
	}
}
