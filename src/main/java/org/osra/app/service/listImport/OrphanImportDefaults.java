package org.osra.app.service.listImport;

import org.osra.app.domain.Orphan;
import org.osra.app.domain.OrphanList;
import org.osra.app.domain.lookup.OrphanStatus;


public class OrphanImportDefaults {
	
	private OrphanList l;
	
	public OrphanImportDefaults(OrphanList l) {		
		this.l = l;
	}

	public void applyDefaults(Orphan o) {
		o.setList(l);
		o.setStatus(OrphanStatus.ON_HOLD);
//		o.setDateOfBirth(new Date());
//		o.setMotherAlive(true);

	}
}
