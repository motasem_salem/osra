package org.osra.app.service.listImport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.osra.app.domain.Orphan;
import org.osra.app.domain.lookup.Gender;
import org.osra.app.domain.lookup.Governorate;

public final class OrphanListImportService {

	/*
	 * public static List<Orphan> getAllOrphans(OrphanListDataSource olds)
	 * throws Exception { ArrayList<Orphan> orphans = new ArrayList<Orphan>();
	 * Orphan o; olds.init(); while(olds.hasNext()) { o = olds.next();
	 * orphans.add(o); } return orphans; }
	 */
	public static OrphanListImportResults validateAndImport(
			OrphanListDataSource olds) {
		OrphanListImportResults results = new OrphanListImportResults();

		try {
			olds.init();
		} catch (ImportException e) {
			results.addValidationError(e);
			return results;
		}
		OrphanDataRecord rec;
		OrphanListImportResults recResults;
		while (olds.hasNext()) {
			rec = olds.next();
			recResults = processRecord(rec);
			// if (!recResults.getValidationErrors().isEmpty())
			// results.addMultiValidationError(recResults.getValidationErrors());
			results.getValidationErrors().addAll(
					recResults.getValidationErrors());
			// if (!results.getOrphans().isEmpty())
			results.getOrphans().addAll(recResults.getOrphans());
		}
		return results;
	}

	private static OrphanListImportResults processRecord(OrphanDataRecord r) {
		OrphanListImportResults results = new OrphanListImportResults();
		Orphan o = new Orphan();
		ArrayList<ImportException> errors = new ArrayList<ImportException>();

		OrphanDataField f;

		/*
		 * f = r.getContact1Field(); try { String s = f.getStringValue(true); //
		 * o.setContact1(s); } catch (Exception e) { errors.add(new
		 * ImportException(f.getReference(), e.getMessage())); }
		 * 
		 * f = r.getContact2Field(); try { String s = f.getStringValue(false);
		 * // o.setContact2(s); } catch (Exception e) { // Nothing. not required
		 * }
		 */

		f = r.getFirstNameField();
		try {
			String s = f.getStringValue(true);
			o.setFirstName(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(), "First Name: "
					+ e.getMessage()));
		}

		f = r.getFatherNameField();
		try {
			String s = f.getStringValue(true);
			o.setFatherName(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(), "Father Name: "
					+ e.getMessage()));
		}

		f = r.getFatherMartyrField();
		try {
			boolean s = f.getBooleanValue(true);
			o.setFatherMartyr(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Is Father Martyr: " + e.getMessage()));
		}

		f = r.getFatherOccupationField();
		try {
			String s = f.getStringValue(false);
			o.setFatherOccupation(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getFatherPlaceOfDeathField();
		try {
			String s = f.getStringValue(false);
			o.setFatherPlaceOfDeath(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getFatherCauseOfDeathField();
		try {
			String s = f.getStringValue(false);
			o.setFatherCauseOfDeath(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getFatherDateOfDeathField();
		try {
			Date s = f.getDateValue(true);
			o.setFatherDateOfDeath(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Father Date of Death: " + e.getMessage()));
		}

		f = r.getMotherNameField();
		try {
			String s = f.getStringValue(true);
			o.setMotherName(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(), "Mother Name: "
					+ e.getMessage()));
		}

		f = r.getMotherAliveField();
		try {
			boolean s = f.getBooleanValue(true);
			o.setMotherAlive(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Is Mother Alive: " + e.getMessage()));
		}

		f = r.getDateOfBirthField();
		try {
			Date s = f.getDateValue(true);
			o.setDateOfBirth(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(), "Date of Birth: "
					+ e.getMessage()));
		}

		f = r.getGenderField();
		try {
			Gender s = f.getGenderValue();
			o.setGender(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(), "Gender: "
					+ e.getMessage()));
		}

		f = r.getHealthStatusField();
		try {
			String s = f.getStringValue(false);
			o.setHealthStatus(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getSchoolingStatusField();
		try {
			String s = f.getStringValue(false);
			o.setSchoolingStatus(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getGoesToSchoolField();
		try {
			boolean s = f.getBooleanValue(false);
			o.setGoesToSchool(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getGuardianNameField();
		try {
			String s = f.getStringValue(true);
			o.setGuardianName(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(), "Guardian Name: "
					+ e.getMessage()));
		}

		f = r.getGuardianRelationshipField();
		try {
			String s = f.getStringValue(true);
			o.setGuardianRelationship(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Guardian Relationship: " + e.getMessage()));
		}

		f = r.getGuardianIDNumberField();
		try {
			long s = f.getLongValue(true);
			o.setGuardianIDNumber(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Guardian ID Number: " + e.getMessage()));
		}

		f = r.getOriginalAddressGovField();
		try {
			Governorate s = f.getGovernorate();
			o.setOriginalAddressGov(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Original Address Governorate: " + e.getMessage()));
		}

		f = r.getOriginalAddressCityField();
		try {
			String s = f.getStringValue(true);
			o.setOriginalAddressCity(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Original Address City: " + e.getMessage()));
		}

		f = r.getOriginalAddressNeighborhoodField();
		try {
			String s = f.getStringValue(true);
			o.setOriginalAddressNeighborhood(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Original Address Neighborhood: " + e.getMessage()));
		}

		f = r.getOriginalAddressStreetField();
		try {
			String s = f.getStringValue(false);
			o.setOriginalAddressStreet(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getOriginalAddressDetailsField();
		try {
			String s = f.getStringValue(false);
			o.setOriginalAddressDetails(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getCurrentAddressGovField();
		try {
			Governorate s = f.getGovernorate();
			o.setCurrentAddressGov(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Current Address Governorate: " + e.getMessage()));
		}

		f = r.getCurrentAddressCityField();
		try {
			String s = f.getStringValue(true);
			o.setCurrentAddressCity(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Original Address City: " + e.getMessage()));
		}

		f = r.getCurrentAddressNeighborhoodField();
		try {
			String s = f.getStringValue(true);
			o.setCurrentAddressNeighborhood(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Original Address Neighborhood: " + e.getMessage()));
		}

		f = r.getCurrentAddressStreetField();
		try {
			String s = f.getStringValue(false);
			o.setCurrentAddressStreet(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getCurrentAddressDetailsField();
		try {
			String s = f.getStringValue(false);
			o.setCurrentAddressDetails(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getContactNumberField();
		try {
			String s = f.getStringValue(true);
			o.setContactNumber(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(), "Contact Number: "
					+ e.getMessage()));
		}

		f = r.getAlternativeContactNumberField();
		try {
			String s = f.getStringValue(false);
			o.setAlternativeContactNumber(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getSponsoredByAnotherOrgField();
		try {
			boolean s = f.getBooleanValue(true);
			o.setSponsoredByAnotherOrg(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Is Sponsored by Another Organization: " + e.getMessage()));
		}

		f = r.getAnotherOrgSponsorshipDetailsField();
		try {
			String s = f.getStringValue(false);
			o.setAnotherOrgSponsorshipDetails(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getMinorSiblingsCountField();
		try {
			int s = f.getIntegerValue(true);
			o.setMinorSiblingsCount(s);
		} catch (Exception e) {
			errors.add(new ImportException(f.getReference(),
					"Number of Minor Siblings: " + e.getMessage()));
		}

		f = r.getSponsoredMinorSiblinbgsCountField();
		try {
			int s = f.getIntegerValue(false);
			o.setSponsoredMinorSiblinbgsCount(s);
		} catch (Exception e) {
			// Not required
		}

		f = r.getCommentsField();
		try {
			String s = f.getStringValue(false);
			o.setComments(s);
		} catch (Exception e) {
			// Not required
		}

		if (errors.isEmpty())
			results.addOrphan(o);
		else
			results.addMultiValidationError(errors);
		return results;
	}

	public static void applyImportDefaults(List<Orphan> orphans,
			OrphanImportDefaults defs) {
		for (Orphan o : orphans)
			defs.applyDefaults(o);
	}

}
