package org.osra.app.service.listImport;

import java.util.Date;

import org.osra.app.domain.lookup.Gender;
import org.osra.app.domain.lookup.Governorate;



public interface OrphanDataField  {
	
	public String getReference();
	
	public int getIntegerValue(boolean required) throws Exception;
	
	public long getLongValue(boolean required) throws Exception;
	
	public String getStringValue(boolean required) throws Exception;
	
	public Gender getGenderValue() throws Exception;
	
	public Governorate getGovernorate() throws Exception;
	 
	public Date getDateValue(boolean required) throws Exception;
	
	public boolean getBooleanValue(boolean required) throws Exception;
	
}
