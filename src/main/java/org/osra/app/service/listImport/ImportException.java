package org.osra.app.service.listImport;

public class ImportException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String exLocationReference;
	private String exDetailMsg;
	
	public ImportException(String exLocationReference, String exDetailMsg) {
		super();
		this.exLocationReference = exLocationReference;
		this.exDetailMsg = exDetailMsg;
	}
	public String getExLocationReference() {
		return exLocationReference;
	}
	public String getExDetailMsg() {
		return exDetailMsg;
	}
	
	
	
	
}
