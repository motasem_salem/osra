package org.osra.app.service.listImport;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.osra.app.domain.Orphan;

public class OrphanListExcelDataSource implements OrphanListDataSource {
	
	private final static int START_ROW=3;
	private final static int START_COL=2;
	
	private int cursor = START_ROW;
	
	private byte[] xlsFileBytes;
	
	private Sheet sheet;
	
	private int lastRow=0;
	
	public OrphanListExcelDataSource(byte[] xlsFileBytes) {
	//	super();
		this.xlsFileBytes = xlsFileBytes;
	}

	@Override
	public void init() throws ImportException {
		ByteArrayInputStream bais = new ByteArrayInputStream(xlsFileBytes);
		
		try {
			sheet = WorkbookFactory.create (bais).getSheetAt(0);
		} catch (Exception e) {
			throw new ImportException("Import File",e.getMessage());
		}
		lastRow = sheet.getLastRowNum();	
		validateSheet();
		if(!hasNext()) throw new ImportException("Import File","File does not contain any Orphan records");
	}
	
	private void validateSheet() throws ImportException {
		for(int i=0;i<lastRow;++i) {
			if(sheet.getRow(i)==null) throw new ImportException("Row "+(i+1), "Row is empty while sheet has "+(lastRow+1)+" rows.");
		}		
	}

	private Orphan convert(Row row) {
		Orphan o = new Orphan();
//		o.setName(row.getCell(0).getStringCellValue());
//		o.setRegion(row.getCell(3).getStringCellValue());
//		o.setFatherName(row.getCell(7).getStringCellValue());
//		o.setMotherName(row.getCell(8).getStringCellValue());
//		o.setCauseOfDeath(row.getCell(9).getStringCellValue());
//		o.setGuardian(row.getCell(11).getStringCellValue());
//		o.setAddress(row.getCell(12).getStringCellValue());
//		o.setContact1(row.getCell(13).getStringCellValue());
//		o.setContact2(row.getCell(14).getStringCellValue());
//		o.setAdditionalInfo(row.getCell(15).getStringCellValue());
//		o.setHealthStatusDescription(row.getCell(17).getStringCellValue());
		
		//row.cellIterator()
		return o;

	}

	@Override
	public boolean hasNext() {
		return cursor <= lastRow;
		
	}

	@Override
	public OrphanDataRecord next() {		
		OrphanExcelDataRecord r = new OrphanExcelDataRecord(sheet.getRow(cursor));
		++cursor;
		return r;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
}
