package org.osra.app.service.listImport;

import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.osra.app.domain.lookup.Gender;
import org.osra.app.domain.lookup.Governorate;

public class OrphanExcelDataField implements OrphanDataField {

	Cell myCell;

	public OrphanExcelDataField(Cell myCell) {
		super();
		this.myCell = myCell;
	}

	@Override
	public String getReference() {
		return "Cell(" + myCell.getRowIndex() + "," + myCell.getColumnIndex()
				+ ")";
	}

	@Override
	public int getIntegerValue(boolean required) {
		return Double.valueOf(myCell.getNumericCellValue()).intValue();
		// return -1;
	}

	@Override
	public String getStringValue(boolean required) throws Exception {
		String s = myCell.getStringCellValue();
		if (required && s.isEmpty())
			throw new Exception("Required String Value is empty");
		return s;
	}

	@Override
	public Date getDateValue(boolean required) {
		// TODO Auto-generated method stub
		return new Date();
	}

	@Override
	public boolean getBooleanValue(boolean required) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Gender getGenderValue() throws Exception {
		// TODO Auto-generated method stub
		return Gender.MALE;
	}

	@Override
	public Governorate getGovernorate() throws Exception {
		// TODO Auto-generated method stub
		return Governorate.DAMASCUS;
	}

	@Override
	public long getLongValue(boolean required) throws Exception {
		return Double.valueOf(myCell.getNumericCellValue()).longValue();

	}
}
