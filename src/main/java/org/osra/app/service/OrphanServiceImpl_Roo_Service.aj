// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package org.osra.app.service;

import java.util.List;
import org.osra.app.domain.Orphan;
import org.osra.app.service.OrphanServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

privileged aspect OrphanServiceImpl_Roo_Service {
    
    declare @type: OrphanServiceImpl: @Service;
    
    declare @type: OrphanServiceImpl: @Transactional;
    
    public long OrphanServiceImpl.countAllOrphans() {
        return Orphan.countOrphans();
    }
    
    public void OrphanServiceImpl.deleteOrphan(Orphan orphan) {
        orphan.remove();
    }
    
    public Orphan OrphanServiceImpl.findOrphan(Long id) {
        return Orphan.findOrphan(id);
    }
    
    public List<Orphan> OrphanServiceImpl.findAllOrphans() {
        return Orphan.findAllOrphans();
    }
    
    public List<Orphan> OrphanServiceImpl.findOrphanEntries(int firstResult, int maxResults) {
        return Orphan.findOrphanEntries(firstResult, maxResults);
    }
    
    public void OrphanServiceImpl.saveOrphan(Orphan orphan) {
        orphan.persist();
    }
    
    public Orphan OrphanServiceImpl.updateOrphan(Orphan orphan) {
        return orphan.merge();
    }
    
}
