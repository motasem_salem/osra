// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package org.osra.app.service;

import java.util.List;
import org.osra.app.domain.OrphanList;
import org.osra.app.service.OrphanListService;

privileged aspect OrphanListService_Roo_Service {
    
    public abstract long OrphanListService.countAllOrphanLists();    
    public abstract void OrphanListService.deleteOrphanList(OrphanList orphanList);    
    public abstract OrphanList OrphanListService.findOrphanList(Long id);    
    public abstract List<OrphanList> OrphanListService.findAllOrphanLists();    
    public abstract List<OrphanList> OrphanListService.findOrphanListEntries(int firstResult, int maxResults);    
    public abstract void OrphanListService.saveOrphanList(OrphanList orphanList);    
    public abstract OrphanList OrphanListService.updateOrphanList(OrphanList orphanList);    
}
