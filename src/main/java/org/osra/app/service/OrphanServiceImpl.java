package org.osra.app.service;

import java.util.List;

import org.osra.app.domain.Orphan;

public class OrphanServiceImpl implements OrphanService {

	@Override
	public void saveOrphans(List<Orphan> orphans) {
		for (Orphan o : orphans)
			saveOrphan(o);
	}
}
