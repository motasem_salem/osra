package org.osra.app.service;

import java.util.List;

import org.osra.app.domain.Orphan;
import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { org.osra.app.domain.Orphan.class })
public interface OrphanService {
	void saveOrphans(List<Orphan> orphans);
}
