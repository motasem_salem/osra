package org.osra.app.service;
import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { org.osra.app.domain.Sponsor.class })
public interface SponsorService {
}
