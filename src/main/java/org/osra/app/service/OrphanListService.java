package org.osra.app.service;
import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { org.osra.app.domain.OrphanList.class })
public interface OrphanListService {
}
