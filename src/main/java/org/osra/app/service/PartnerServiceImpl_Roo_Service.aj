// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package org.osra.app.service;

import java.util.List;
import org.osra.app.domain.Partner;
import org.osra.app.service.PartnerServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

privileged aspect PartnerServiceImpl_Roo_Service {
    
    declare @type: PartnerServiceImpl: @Service;
    
    declare @type: PartnerServiceImpl: @Transactional;
    
    public long PartnerServiceImpl.countAllPartners() {
        return Partner.countPartners();
    }
    
    public void PartnerServiceImpl.deletePartner(Partner partner) {
        partner.remove();
    }
    
    public Partner PartnerServiceImpl.findPartner(Long id) {
        return Partner.findPartner(id);
    }
    
    public List<Partner> PartnerServiceImpl.findAllPartners() {
        return Partner.findAllPartners();
    }
    
    public List<Partner> PartnerServiceImpl.findPartnerEntries(int firstResult, int maxResults) {
        return Partner.findPartnerEntries(firstResult, maxResults);
    }
    
    public void PartnerServiceImpl.savePartner(Partner partner) {
        partner.persist();
    }
    
    public Partner PartnerServiceImpl.updatePartner(Partner partner) {
        return partner.merge();
    }
    
}
