package org.osra.app.web.zk;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.osra.app.domain.Orphan;
import org.osra.app.domain.lookup.Gender;
import org.osra.app.domain.lookup.Governorate;
import org.osra.app.domain.lookup.OrphanStatus;
import org.osra.app.service.OrphanService;
import org.osra.app.web.util.SessionUtilBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;

@Configurable
public class OrphanVM implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private OrphanService orphanService;
	
	@Autowired
	private SessionUtilBean sessionUtilBean;
	
	private Orphan myOrphan;
	
	public Orphan getMyOrphan() {
		return myOrphan;
	}
	
	public List<Gender> getGenderList() {
		return Arrays.asList(Gender.values());
	}
	
	public List<Governorate> getGovernorateList() {
		return Arrays.asList(Governorate.values());
	}
	
	public List<OrphanStatus> getOrphanStatusList() {
		return Arrays.asList(OrphanStatus.values());
	}

	@Init
	public void init() {
		myOrphan = sessionUtilBean.getSelectedOrphan();
	}
	
	@Command
	@NotifyChange("myOrphan")
	public void updateOrphan() {
		try {
			orphanService.updateOrphan(myOrphan);
		} catch (Exception e) {
			Clients.alert("Error " + e);
			return;
		}
		Clients.alert("Orphan UPDATED!!!! with ID: " + myOrphan.getId());
		myOrphan = new Orphan();
//		Executions.createComponent
	}
}
