package org.osra.app.web.zk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.osra.app.domain.Orphan;
import org.osra.app.domain.OrphanList;
import org.osra.app.domain.Partner;
import org.osra.app.service.OrphanListService;
import org.osra.app.service.OrphanService;
import org.osra.app.service.listImport.ImportException;
import org.osra.app.service.listImport.OrphanImportDefaults;
import org.osra.app.service.listImport.OrphanListExcelDataSource;
import org.osra.app.service.listImport.OrphanListImportResults;
import org.osra.app.service.listImport.OrphanListImportService;
import org.osra.app.web.util.SessionUtilBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.util.Clients;

@Configurable
public class OrphanListVM implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private OrphanService orphanService;
	
	@Autowired
	private OrphanListService orphanListService;

	@Autowired
	private SessionUtilBean sessionUtilBean;

	private Media file;

	private List<Orphan> orphans;

	private List<ImportException> validationErrors;

	private OrphanList list;

	public SessionUtilBean getSessionUtilBean() {
		return sessionUtilBean;
	}

	public Media getFile() {
		return file;
	}

	public List<Orphan> getOrphans() {
		return orphans;
	}

	public List<ImportException> getValidationErrors() {
		return validationErrors;
	}

	@Init
	public void init() {
		file = null;
		orphans = new ArrayList<Orphan>();
		validationErrors = new ArrayList<ImportException>();
		list = new OrphanList();
	}

	@Command
	@NotifyChange({ "file", "orphans", "validationErrors" })
	public void upload(
			@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {

		init();
		try {
			file = event.getMedia();
			byte[] xlsFileBytes = file.getByteData();
			OrphanListExcelDataSource olds = new OrphanListExcelDataSource(
					xlsFileBytes);
			OrphanListImportResults results = OrphanListImportService
					.validateAndImport(olds);
			if (results.getValidationErrors().isEmpty()) {
				orphans = results.getOrphans();
				list.setFile(xlsFileBytes);
				list.setFileName(file.getName());
				list.setOrphanCount(orphans.size());
				list.setPartner(sessionUtilBean.getSelectedPartner());
			} else
				validationErrors = results.getValidationErrors();
		} catch (Exception e) {
			validationErrors.add(new ImportException("Import File",
					"Error reading file: " + e));
		}
	}

	@Command
	@NotifyChange("*")
	public void importList() {
		orphanListService.saveOrphanList(list);
		OrphanImportDefaults defs = new OrphanImportDefaults(list);
		OrphanListImportService.applyImportDefaults(orphans, defs);
		orphanService.saveOrphans(orphans);
		Clients.alert(list.getOrphanCount()+" Orphans Imported successfully!!!!");
		System.out.println("#### done importing!!!");
		init();	
	}

}
