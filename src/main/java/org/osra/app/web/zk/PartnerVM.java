package org.osra.app.web.zk;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.osra.app.domain.Partner;
import org.osra.app.domain.lookup.Governorate;
import org.osra.app.domain.lookup.PartnerStatus;
import org.osra.app.domain.lookup.PartnerType;
import org.osra.app.service.PartnerService;
import org.osra.app.web.util.SessionUtilBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.util.Clients;

@Configurable
public class PartnerVM implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private SessionUtilBean sessionUtilBean;

	private Partner myPartner;

	public List<Partner> getAllPartners() {
		return partnerService.findAllPartners();
	}
	
	public List<PartnerType> getPartnerTypeList() {
		return Arrays.asList(PartnerType.values());
	}
	
	public List<PartnerStatus> getPartnerStatusList() {
		return Arrays.asList(PartnerStatus.values());
	}
	
	public List<Governorate> getGovernorateList() {
		return Arrays.asList(Governorate.values());
	}

	public Partner getMyPartner() {
		return myPartner;
	}

	public void setMyPartner(Partner myPartner) {
		this.myPartner = myPartner;
	}

	public boolean isPartnerSelected() {
		return myPartner.getId() != null;
	}

	@Init
	public void init() {
		myPartner = new Partner();
		sessionUtilBean.setSelectedPartner(myPartner);
		// partnerDetailsVisible = false;
		// importListVisible = false;
		// logger = Logger.getLogger(PartnerViewModel.class);
		// logger.fatal("Initialized");
		// myDummy = new Dummy();
		// logger.fatal("usign autowired:" + sessionUtilBean);
	}

	@Command
	@NotifyChange({ "myPartner", "allPartners" })
	public void savePartner() {
		try {
			partnerService.savePartner(myPartner);
		} catch (Exception e) {

			Clients.alert("Error " + e);
			return;
		}

		Clients.alert("Partner SAVED!!!! with ID: " + myPartner.getId());
		myPartner = new Partner();
	}

	@Command
	@NotifyChange({ "myPartner", "allPartners", "partnerSelected" })
	public void updatePartner() {
		try {
			partnerService.updatePartner(myPartner);
		} catch (Exception e) {
			Clients.alert("Error " + e);
			return;
		}

		Clients.alert("Partner UPDATED!!!! with ID: " + myPartner.getId());
		myPartner = new Partner();
	}

	@Command
	@NotifyChange({ "partnerSelected", "myPartner" })
	public void selectPartner() {
		sessionUtilBean.setSelectedPartner(myPartner);
	}

}
