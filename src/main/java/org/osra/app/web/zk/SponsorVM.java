package org.osra.app.web.zk;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.osra.app.domain.Partner;
import org.osra.app.domain.Sponsor;
import org.osra.app.domain.lookup.Gender;
import org.osra.app.domain.lookup.Governorate;
import org.osra.app.domain.lookup.PartnerStatus;
import org.osra.app.domain.lookup.PartnerType;
import org.osra.app.domain.lookup.SponsorPaymentScheme;
import org.osra.app.domain.lookup.SponsorStatus;
import org.osra.app.domain.lookup.SponsorType;
import org.osra.app.domain.lookup.YesNo;
import org.osra.app.service.PartnerService;
import org.osra.app.service.SponsorService;
import org.osra.app.web.util.SessionUtilBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.util.Clients;

@Configurable
public class SponsorVM implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private SponsorService sponsorService;

	@Autowired
	private SessionUtilBean sessionUtilBean;

	private Sponsor mySponsor;

	public List<Sponsor> getAllSponsors() {
		return sponsorService.findAllSponsors();
	}
	
	public List<SponsorType> getSponsorTypeList() {
		return Arrays.asList(SponsorType.values());
	}
	
	public List<SponsorStatus> getSponsorStatusList() {
		return Arrays.asList(SponsorStatus.values());
	}
	
	public List<Gender> getGenderList() {
		return Arrays.asList(Gender.values());
	}	
		
	public List<SponsorPaymentScheme> getPaymentSchemeList() {
		return Arrays.asList(SponsorPaymentScheme.values());
	}

	public Sponsor getMySponsor() {
		return mySponsor;
	}

	public void setMySponsor(Sponsor mySponsor) {
		this.mySponsor = mySponsor;
	}

	public boolean isPartnerSelected() {
		return mySponsor.getId() != null;
	}

	@Init
	public void init() {
		mySponsor = new Sponsor();
		sessionUtilBean.setSelectedSponsor(mySponsor);
		// partnerDetailsVisible = false;
		// importListVisible = false;
		// logger = Logger.getLogger(PartnerViewModel.class);
		// logger.fatal("Initialized");
		// myDummy = new Dummy();
		// logger.fatal("usign autowired:" + sessionUtilBean);
	}
	
	@Command
	@NotifyChange({ "mySponsor", "allSponsors" })
	public void saveSponsor() {
		mySponsor.setRequestFulfilled(YesNo.NO);
		try {
			sponsorService.saveSponsor(mySponsor);
		} catch (Exception e) {

			Clients.alert("Error " + e);
			return;
		}

		Clients.alert("Sponsor SAVED!!!! with ID: " + mySponsor.getId());
		mySponsor = new Sponsor();
	}



	@Command
	@NotifyChange({ "mySposnor", "allSponsors", "sponsorSelected" })
	public void updateSponsor() {
		try {
			sponsorService.updateSponsor(mySponsor);
		} catch (Exception e) {
			Clients.alert("Error " + e);
			return;
		}

		Clients.alert("Sponsor UPDATED!!!! with ID: " + mySponsor.getId());
		mySponsor = new Sponsor();
	}

	@Command
	@NotifyChange({ "sponsorSelected", "mySponsor" })
	public void selectSponsor() {
		sessionUtilBean.setSelectedSponsor(mySponsor);
	}

}
