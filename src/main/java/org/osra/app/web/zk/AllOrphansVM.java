package org.osra.app.web.zk;

import java.io.Serializable;
import java.util.List;

import org.osra.app.domain.Orphan;
import org.osra.app.domain.Partner;
import org.osra.app.service.OrphanService;
import org.osra.app.web.util.SessionUtilBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;

@Configurable
public class AllOrphansVM implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private OrphanService orphanService;
	
	@Autowired
	private SessionUtilBean sessionUtilBean;
	
	private Orphan myOrphan;
	
	public List<Orphan> getAllOrphans() {
		return orphanService.findAllOrphans();
	}
	
	public Orphan getMyOrphan() {
		return myOrphan;
	}
	
	public void setMyOrphan(Orphan myOrphan) {
		this.myOrphan = myOrphan;
	}

	@Init
	public void init() {
		myOrphan = null;
		sessionUtilBean.setSelectedOrphan(myOrphan);
	}
	
	@Command
	public void selectOrphan() {
		sessionUtilBean.setSelectedOrphan(myOrphan);		
	}
}
