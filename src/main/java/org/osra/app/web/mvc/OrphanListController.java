package org.osra.app.web.mvc;
import org.osra.app.domain.OrphanList;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/orphanlists")
@Controller
@RooWebScaffold(path = "orphanlists", formBackingObject = OrphanList.class)
public class OrphanListController {
}
