package org.osra.app.web.mvc;
import org.osra.app.domain.Orphan;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/orphans")
@Controller
@RooWebScaffold(path = "orphans", formBackingObject = Orphan.class)
public class OrphanController {
}
