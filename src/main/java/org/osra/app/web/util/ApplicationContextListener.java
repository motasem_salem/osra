package org.osra.app.web.util;

import org.osra.app.domain.DBInitializer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextListener implements
		ApplicationListener<ContextRefreshedEvent> {

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		System.out
				.println("########################## Application Context Listener invoked");
		DBInitializer.initDB();
	}
}
