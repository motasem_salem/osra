package org.osra.app.web.util;

import org.osra.app.domain.Orphan;
import org.osra.app.domain.Partner;
import org.osra.app.domain.Sponsor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class SessionUtilBean {

	private Partner selectedPartner;

	private Orphan selectedOrphan;
	
	private Sponsor selectedSponsor;

	public Orphan getSelectedOrphan() {
		return selectedOrphan;
	}

	public void setSelectedOrphan(Orphan selectedOrphan) {
		this.selectedOrphan = selectedOrphan;
	}

	public Partner getSelectedPartner() {
		return selectedPartner;
	}

	public void setSelectedPartner(Partner selectedPartner) {
		this.selectedPartner = selectedPartner;
	}
	
	public void setSelectedSponsor(Sponsor selectedSponsor) {
		this.selectedSponsor = selectedSponsor;
	}

}
