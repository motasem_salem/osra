package org.osra.app.domain;

import java.util.Date;

import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.osra.app.domain.lookup.Governorate;
import org.osra.app.domain.lookup.PartnerStatus;
import org.osra.app.domain.lookup.PartnerType;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Partner {

	@NotNull
	private String name;

	@NotNull
	private PartnerType type;

	/**
     */
	@NotNull
	private Governorate governorate;

	@NotNull
	private String region;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "M-")
	private Date partnershipDate;

	@NotNull
	@Enumerated
	private PartnerStatus status;
}
