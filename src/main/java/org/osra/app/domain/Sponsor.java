package org.osra.app.domain;
import java.util.Date;

import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.osra.app.domain.lookup.Gender;
import org.osra.app.domain.lookup.SponsorPaymentScheme;
import org.osra.app.domain.lookup.SponsorStatus;
import org.osra.app.domain.lookup.SponsorType;
import org.osra.app.domain.lookup.YesNo;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Sponsor {
	
	@NotNull
	private String name;
	
	@NotNull
	@Enumerated
	private SponsorStatus status;
	
	@NotNull
	@Enumerated
	private SponsorType type;
	
	@Enumerated
	private Gender gender;
	
	@NotNull
	private String address;
	
	// TODO: Country enumeration
	
	@NotNull
	private String country;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "M-")
	private Date sponsorshipDate;
	
	@NotNull
	@Enumerated
	private SponsorPaymentScheme paymentScheme;
	
	private String paymentDescription;
	
	@NotNull
	private String contact1;
	
	private String contact2;
	
	@Email
	private String email;
	
	private String additionalInfo;
	
	@ManyToOne
	private Organization organization;
	
	@NotNull
	private YesNo requestFulfilled;
	
	// TODO: link to user
	
	
	
	
	
	
	
	
}
