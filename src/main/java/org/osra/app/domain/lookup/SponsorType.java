package org.osra.app.domain.lookup;

public enum SponsorType implements LookupValue {

	INDIVIDUAL(0), ORGANIZATION(1);

	private final Integer code;

	private SponsorType(Integer code) {
		this.code = code;
	}

	@Override
	public Integer getCode() {
		return code;
	}

	public static SponsorType getValue(Integer code) {

		if (code == null) {
			return null;
		}

		for (SponsorType s : SponsorType.values()) {
			if (code.equals(s.getCode())) {
				return s;
			}
		}
		throw new IllegalArgumentException("No matching type for code " + code);
	}
}
