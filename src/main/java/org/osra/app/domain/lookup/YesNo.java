package org.osra.app.domain.lookup;


public enum YesNo implements LookupValue {  
    YES(0),  
    NO(1);  
   
    private final Integer code;
    
    private YesNo(Integer code) {  
        this.code = code;  
    }  
    
    @Override
    public Integer getCode() {  
        return code;  
    } 
    
    
    public static YesNo getValue(Integer code) {
        
        if (code == null) {
            return null;
        }

        for (YesNo b : YesNo.values()) {
            if (code.equals(b.getCode())) {
                return b;
            }
        }
        throw new IllegalArgumentException("No matching type for code " + code);
    }

}  