package org.osra.app.domain.lookup;

public enum Governorate implements LookupValue {

	DAMASCUS(0), HOMS(1);

	private final Integer code;

	private Governorate(Integer code) {
		this.code = code;
	}

	@Override
	public Integer getCode() {
		return code;
	}

	public static Governorate getValue(Integer code) {

		if (code == null) {
			return null;
		}

		for (Governorate g : Governorate.values()) {
			if (code.equals(g.getCode())) {
				return g;
			}
		}
		throw new IllegalArgumentException("No matching type for code " + code);
	}

}
