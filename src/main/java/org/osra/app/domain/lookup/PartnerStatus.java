package org.osra.app.domain.lookup;

public enum PartnerStatus implements LookupValue {

	ACTIVE(0), INACTIVE(1), ON_HOLD(2), UNDER_REVISION(3);

	private final Integer code;

	private PartnerStatus(Integer code) {
		this.code = code;
	}

	@Override
	public Integer getCode() {
		return code;
	}

	public static PartnerStatus getValue(Integer code) {

		if (code == null) {
			return null;
		}

		for (PartnerStatus s : PartnerStatus.values()) {
			if (code.equals(s.getCode())) {
				return s;
			}
		}
		throw new IllegalArgumentException("No matching type for code " + code);
	}
}
