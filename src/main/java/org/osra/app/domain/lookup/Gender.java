package org.osra.app.domain.lookup;


public enum Gender implements LookupValue {  
    MALE(0),  
    FEMALE(1);  
     
    private final Integer code;
    
    private Gender(Integer code) {  
        this.code = code;  
    }  
    
    @Override
    public Integer getCode() {  
        return code;  
    } 
    
    
    public static Gender getValue(Integer code) {
        
        if (code == null) {
            return null;
        }

        for (Gender g : Gender.values()) {
            if (code.equals(g.getCode())) {
                return g;
            }
        }
        throw new IllegalArgumentException("No matching type for code " + code);
    }

}  