package org.osra.app.domain.lookup;

public enum PartnerType implements LookupValue {
	
	INDIVIDUAL(0), ORGANIZATION(1);
	
	private final Integer code;

	private PartnerType(Integer code) {
		this.code = code;
	}

	@Override
	public Integer getCode() {
		return code;
	}

	public static PartnerType getValue(Integer code) {

		if (code == null) {
			return null;
		}

		for (PartnerType t : PartnerType.values()) {
			if (code.equals(t.getCode())) {
				return t;
			}
		}
		throw new IllegalArgumentException("No matching type for code " + code);
	}

}
