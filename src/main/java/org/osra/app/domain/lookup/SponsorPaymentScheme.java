package org.osra.app.domain.lookup;

public enum SponsorPaymentScheme implements LookupValue {

	MONTHLY(0), QUARTERLY(1), ANNUALLY(2), OTHER(3);

	private final Integer code;

	private SponsorPaymentScheme(Integer code) {
		this.code = code;
	}

	@Override
	public Integer getCode() {
		return code;
	}

	public static SponsorPaymentScheme getValue(Integer code) {

		if (code == null) {
			return null;
		}

		for (SponsorPaymentScheme s : SponsorPaymentScheme.values()) {
			if (code.equals(s.getCode())) {
				return s;
			}
		}
		throw new IllegalArgumentException("No matching type for code " + code);
	}
}
