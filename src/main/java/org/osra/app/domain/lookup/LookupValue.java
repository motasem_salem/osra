package org.osra.app.domain.lookup;

public interface LookupValue {

	Integer getCode();	

}
