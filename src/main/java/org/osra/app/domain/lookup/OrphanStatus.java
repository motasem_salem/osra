package org.osra.app.domain.lookup;

public enum OrphanStatus implements LookupValue {

	ACTIVE(0), INACTIVE(1), ON_HOLD(2), UNDER_REVISION(3);

	private final Integer code;

	private OrphanStatus(Integer code) {
		this.code = code;
	}

	@Override
	public Integer getCode() {
		return code;
	}

	public static OrphanStatus getValue(Integer code) {

		if (code == null) {
			return null;
		}

		for (OrphanStatus s : OrphanStatus.values()) {
			if (code.equals(s.getCode())) {
				return s;
			}
		}
		throw new IllegalArgumentException("No matching type for code " + code);
	}

}
