package org.osra.app.domain;

import java.util.Date;

import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Column;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.osra.app.domain.lookup.Gender;
import org.osra.app.domain.lookup.Governorate;
import org.osra.app.domain.lookup.OrphanStatus;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Orphan {
	
	@NotNull
	private String firstName;
	
	@NotNull
	private String fatherName;
	
	@NotNull
	private boolean fatherMartyr;
	
	private String fatherOccupation;
	
	private String fatherPlaceOfDeath;
	
	private String fatherCauseOfDeath;
	
	@Past
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "M-") 
	private Date fatherDateOfDeath;
	
	@NotNull
	private String motherName;
	
	@NotNull
	private boolean motherAlive;
	
	
	@NotNull
	@Past
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "M-")
	private Date dateOfBirth;

	@NotNull
	@Enumerated
	private Gender gender;
	
	private String healthStatus;
	
	private String schoolingStatus;
	
	private boolean goesToSchool;
	
	
	@NotNull
	private String guardianName;
	
	@NotNull
	private String guardianRelationship;
	
	@NotNull
	private Long guardianIDNumber;
	
	
	@NotNull
	@Enumerated
	private Governorate originalAddressGov;
	
	@NotNull
	private String originalAddressCity;
	
	@NotNull
	private String originalAddressNeighborhood;
	
	private String originalAddressStreet;
	
	private String originalAddressDetails;
	
	
	@NotNull
	@Enumerated
	private Governorate currentAddressGov;
	
	@NotNull
	private String currentAddressCity;
	
	@NotNull
	private String currentAddressNeighborhood;
	
	private String currentAddressStreet;
	
	private String currentAddressDetails;
	
	
	@NotNull
	private String contactNumber;
	
	private String alternativeContactNumber;
	
	@NotNull
	private boolean sponsoredByAnotherOrg;
	
	private String anotherOrgSponsorshipDetails;
	
	@NotNull
	private int minorSiblingsCount;
	
	private int sponsoredMinorSiblinbgsCount;

	// TODO: attachments
	// private PHOTO
	// private: family book
	// private: father death certificate
	
	private String comments;
	
	@ManyToOne
	private OrphanList list;

	@NotNull
	@Enumerated
	private OrphanStatus status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "MM")
	@Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date createdOn;


	public String toString() {
		return ReflectionToStringBuilder.toString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
	
}
