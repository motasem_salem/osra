package org.osra.app.domain;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class OrphanList {
	
	@Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
	@Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date submittedOn;
	
	@NotNull
	@ManyToOne
	private Partner partner;
	
	@Lob
	@NotNull
	private byte[] file;
	
	@NotNull
	private String fileName;
	
	private int orphanCount;
	
}
